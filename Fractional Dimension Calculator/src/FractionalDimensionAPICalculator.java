import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

public class FractalDimensionCalculatorAPI 
{
	public static double calculateFractalDimension(BufferedImage image)
	{
		image = getCroppedImage(image);
		Image temp = image.getScaledInstance(2 * image.getWidth(), 2 * image.getHeight(), Image.SCALE_SMOOTH);
	    BufferedImage doubledImage = new BufferedImage(2 * image.getWidth(), 2 * image.getHeight(), BufferedImage.TYPE_INT_ARGB);
	    Graphics2D g2d = doubledImage.createGraphics();
	    g2d.drawImage(temp, 0, 0, null);
	    g2d.dispose();
	    System.out.println("" + getMass(doubledImage) + " " + getMass(image));
	    return Math.log10(56) - Math.log10((float) getMass(doubledImage)/getMass(image))/Math.log10(2);
	}
	private static BufferedImage getCroppedImage(BufferedImage image)
	{
		int startX = 0, startY = 0, endX = 0, endY = 0;
		for(int j = 0; j < image.getWidth(); j++)
			for(int i = 0; i < image.getHeight(); i++)
				if((new Color(image.getRGB(j, i))).equals(Color.BLACK))
				{
					endX = j;
					break;
				}
		for(int j = image.getWidth() - 1; j >= 0; j--)
			for(int i = 0; i < image.getHeight(); i++)
				if((new Color(image.getRGB(j, i))).equals(Color.BLACK))
				{
					startX = j;
					break;
				}
		for(int i = 0; i < image.getHeight(); i++)
			for(int j = 0; j < image.getWidth(); j++)
				if((new Color(image.getRGB(j, i))).equals(Color.BLACK))
				{
					endY = i;
					break;
				}
		for(int i = image.getHeight() - 1; i >= 0; i--)
			for(int j = 0; j < image.getWidth(); j++)
				if((new Color(image.getRGB(j, i))).equals(Color.BLACK))
				{
					startY = i;
					break;
				}
		BufferedImage img = image.getSubimage(startX, startY, endX - startX, endY - startY);
		BufferedImage copyOfImage = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB);
		Graphics g = copyOfImage.createGraphics();
		g.drawImage(img, 0, 0, null);
		return copyOfImage; 
	}
	private static int getMass(BufferedImage image)
	{
		boolean[][] boxes = new boolean[image.getHeight()/2 + 1][image.getWidth()/2 + 1];
		for(int i = 0; i < image.getHeight(); i++)
		{
			for(int j = 0; j < image.getWidth(); j++)
			{
				if((new Color(image.getRGB(j, i))).equals(Color.BLACK)&& 
			        ((j > 0 && !(new Color(image.getRGB(j - 1, i))).equals(Color.BLACK)) || 
			        (j < image.getWidth() - 1 && !(new Color(image.getRGB(j, i))).equals(Color.BLACK))))
				{
					boxes[i/2][j/2] = true;
				}
			}
		}
		int filledBoxes = 0;
		for(boolean [] e : boxes)
			for(boolean box : e)
				if(box)
					filledBoxes++;
		return filledBoxes;
	}
}
