import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
	© 2017 Arjun Nair - All Rights Reserved
	Contact arjunvnair@hotmail.com with any queries or suggestions.
*/

public class Main 
{
	public static void main(String[] args)
	{
		JFrame screen = new JFrame();
		screen.setLayout(new BorderLayout());
		screen.setTitle("Fractal Dimension Calculator");
		BufferedImage image = null;
		try 
		{
			image = ImageIO.read(new File("Norway_map.JPG"));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		screen.add(new JLabel(new ImageIcon(image)), BorderLayout.CENTER);
		WindowAdapter adapter = new WindowAdapter()
		{
			public void windowClosing(WindowEvent arg0) 
			{
				System.exit(0);
			}
		};
		screen.addWindowListener(adapter);
		screen.setSize(image.getWidth(), image.getHeight());
		screen.setVisible(true);
		System.out.printf("%f \n", FractalDimensionCalculatorAPI.calculateFractalDimension(image));
		screen.add(new JLabel("" + FractalDimensionCalculatorAPI.calculateFractalDimension(image)), BorderLayout.SOUTH);
	}
}
